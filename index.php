<?php
require('Init.php');

// se crea el objeto a partir de la clases.
$user = new User;

// dentro de la clase User se crearán los métodos necesarios
//Insertando registro
$newUser = [
	'name' => 'Jorge',
	'last_name' => 'Udes',
	'email' => 'andrew@gmail.com',
	'gender' => 'Male',
	'hobbie' => 'Coding',
	'country' => 'Usa',
];

$usuarioNuew = $user->agregarRegistro($newUser);
echo $usuarioNuew->name . '<br>';
echo $usuarioNuew->last_name . '<br>';
echo $usuarioNuew->email . '<br>';


//Listando registros
echo '<table>
<thead>
  <tr>
	<th>#</th>
	<th>Nambe</th>
	<th>Last</th>
	<th>Email</th>
  </tr>
</thead>
<tbody>';

$i = 1;
foreach ($user->usuarios() as $u) {
	echo '<tr>
			<th scope="row">' . $i . '</th>
			<td>' . $u->name . '</td>
			<td>' . $u->last_name . '</td>
			<td>' . $u->email . '</td>
		</tr>';
	$i++;
}
echo '</tbody>
</table>';

//actualizar un registro
$datosUp['name'] = 'Jorge';
$datosUp['last_name'] = 'Nitales Axules';
$user->actualizarRegistro($datosUp, ['id' => 23]);

//eliminar un registro
$user->eliminarRegistro(4);
