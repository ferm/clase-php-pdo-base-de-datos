<?php

/* Archivo libs/Database.php
 * Clase que maneja la
 * conexión  y comunicación con la
 * base de datos.
 */

class Database
{
	// Datos de acceso de la base de datos de MySQL
	private $host 	= DB_HOST;
	private $user 	= DB_USER;
	private $pass 	= DB_PASS;
	private $dbname = DB_NAME;
	private $dbport = DB_PORT;
	private $charset = DB_CHARSET;

	/* Otras variables:
	* $dbh -> Database Handler
	* $error -> Error Handler
	* $stmt -> statement
	*/
	private $dbh;
	private $error;
	private $stmt;

	// Creando conexión:
	public function __construct()
	{
		// fijar DNS:
		$dns = 'mysql:host=' .  $this->host . ';port=' . $this->dbport . ';dbname=' . $this->dbname . ';charset=' . $this->charset;
		// Estableciendo opciones:
		//PDO::ATTR_PERSISTENT => false,
		$options = array(
			PDO::ATTR_PERSISTENT 				=> false,
			PDO::MYSQL_ATTR_USE_BUFFERED_QUERY 	=> true,
			PDO::ATTR_DEFAULT_FETCH_MODE 		=> PDO::FETCH_ASSOC,
			PDO::ATTR_ERRMODE 					=> PDO::ERRMODE_EXCEPTION
		);

		// Creando nueva instancia del objeto:
		try {
			$this->dbh = new PDO($dns, $this->user, $this->pass, $options);
		} catch (PDOException $e) {
			$this->error = $e->getMessage();
			print "Error en la conexión!: " . $e->getMessage() . "<br>";
			//header('location: bd_error.html');
			die();
		}
	}

	public function query($query)
	{
		$this->stmt = $this->dbh->prepare($query);
	}

	public function bind($param, $value, $type = null)
	{
		if (is_null($type)) {
			switch (true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}
		$this->stmt->bindValue($param, $value, $type);
	}

	public function execute()
	{
		return $this->stmt->execute();
	}

	public function resultSet()
	{
		$this->execute();
		return $this->stmt->fetchAll(PDO::FETCH_OBJ);
	}

	public function resultArray()
	{
		$this->execute();
		return $this->stmt->fetchAll(PDO::FETCH_COLUMN, 0);
	}

	public function singleArray()
	{
		$this->execute();
		return $this->stmt->fetch(PDO::FETCH_COLUMN, 0);
	}

	public function single()
	{
		$this->execute();
		return $this->stmt->fetch(PDO::FETCH_OBJ);
	}

	public function rowCount()
	{
		return $this->stmt->rowCount();
	}

	public function lastInsertId()
	{
		return $this->dbh->lastInsertId();
	}

	public function beginTransaction()
	{
		return $this->dbh->beginTransaction();
	}

	public function endTransaction()
	{
		return $this->dbh->commit();
	}

	public function cancelTransaction()
	{
		return $this->dbh->rollBack();
	}

	/**
	 * insert data to table
	 * @param  string $table table name
	 * @param  array $dat   associative array 'column_name'=>'val'
	 */
	public function insert($table, $dat)
	{
		if ($dat !== null) {
			$data = array_values($dat);
		}
		//grab keys
		$cols = array_keys($dat);
		$col = implode(', ', $cols);

		//grab values and change it value
		$mark = array();
		foreach ($data as $key) {
			$keys = '?';
			$mark[] = $keys;
		}
		$im = implode(', ', $mark);
		$this->stmt = $this->dbh->prepare("INSERT INTO $table ($col) values ($im)");
		if ($this->stmt->execute($data)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * update record
	 * @param  string $table table name
	 * @param  array $dat   associative array 'col'=>'val'
	 * @param  string $id    primary key column name
	 * @param  int $val   key value
	 */
	public function update($table, $dat, $id, $val)
	{
		if ($dat !== null)
			$data = array_values($dat);
		array_push($data, $val);
		//grab keys
		$cols = array_keys($dat);
		$mark = array();
		foreach ($cols as $col) {
			$mark[] = $col . "=?";
		}
		$im = implode(', ', $mark);
		$this->stmt = $this->dbh->prepare("UPDATE $table SET $im where $id=?");
		$this->stmt->execute($data);
	}

	/**
	 * update record
	 * @param  string $table table name
	 * @param  array $dat   associative array 'col'=>'val'
	 * @param  string $id    primary key column name
	 * @param  int $val   key value
	 */
	public function updates($table, $dat, $id = [])
	{
		if ($dat !== null) {
			$data = array_values($dat);
		}
		$key 		= key($id);
		array_push($data, $id[$key]);
		$cols 		= array_keys($dat);
		$mark 		= array();
		foreach ($cols as $col) {
			$mark[] = $col . "=?";
		}
		$im 		= implode(', ', $mark);
		$this->stmt = $this->dbh->prepare("UPDATE $table SET $im where $key = ?");
		if ($this->stmt->execute($data)) {
			return true;
		} else {
			return false;
		}
	}

	public function update_where($table, $datos, $where)
	{
		if ($datos !== null)
			$data = array_values($datos);
		foreach ($where as $w) {
			array_push($data, $w);
		}

		//grab keys
		$cols = array_keys($datos);
		$mark = array();
		foreach ($cols as $col) {
			$mark[] = $col . "=?";
		}
		$im = implode(', ', $mark);

		$cols = array_keys($where);
		$mark = array();
		foreach ($cols as $col) {
			$mark[] = $col . "=?";
		}
		$wh = implode(' and ', $mark);

		$this->stmt = $this->dbh->prepare("UPDATE $table SET $im where $wh");
		$this->stmt->execute($data);
	}

	/**
	 * delete record
	 * @param  string $table table name
	 * @param  string $where column name for condition (commonly primay key column name)
	 * @param   int $id   key value
	 */
	public function delete($table, $where, $id)
	{
		$data = array($id);
		$this->stmt = $this->dbh->prepare("delete from $table where $where=?");
		$this->stmt->execute($data);
	}

	public function deletes($table, $id)
	{
		$data = array_values($id);
		$key 		= key($id);

		$this->stmt = $this->dbh->prepare(" DELETE from $table WHERE $key = ? ");
		if ($this->stmt->execute($data)) {
			return true;
		} else {
			return false;
		}
	}
}
