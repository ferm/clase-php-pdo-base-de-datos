# Clase PHP PDO base de datos (MySQL, MariaDB)


## ¿Qué es?

Cuando se inicia a desarrollar con PHP, definitivamente se requieren proyectos en los que se utilicen con base de datos. Sin embargo, cuando se inicia, no se requiere usar un framework completo o complejo, sino que nos permita realizar consultas, eliminaciones, actualizaciones.

Por lo que aquí comparto una clase simple para realizar ésta tareas, aquí mostraré los ejemplos de cómo crearlos y cómo utilizarlos.

## Requerimientos

- PHP > 7.2
- PDO > 7
- MySQL, MariaDB (Previamente instalar alguna base de datos basadas en mysql)

## Archivos de configuración

### Archivo Config.php
Éste archivo contendrá los acceso para la base de datos.

```
<?php
date_default_timezone_set('America/Mexico_City');

define("DB_HOST",         "localhost");
define("DB_USER",         "root");
define("DB_PASS",         "");
define("DB_NAME",         "base_de_datos");
define("DB_PORT",         "3306");
define("DB_CHARSET",     "latin1");
```

### Archivo Init.php
Éste archivo contendrá las declaraciones de las clases que se creen dentro de la carpeta class y que se requieran en el proyecto.

```
<?php

// Se inicia sesión
session_start();

// Se incluye archivo de configuración
require_once('Config.php');
function autoLoadClass($class)
{
	require_once('classes/'.$class.'.php');
}

spl_autoload_register('autoLoadClass');
```

# Uso
Una vez configurado los archivos Init.php y Config.php, ahora es necesario crear una clase, de preferencia del mismo nombre que la tabla que contenga.

Como ejemplo, se usará la base de datos que viene en éste repositorio:

## Clase User.php
La clase que se cree por cada tabla, contendrá los métodos que se requieran para esa clase: por ejemplo:

```
<?php
class User
{
	// Inicializando variable
	private $db;
	private $table;
	/*
	* Constructor de la clase
	*/

	public function __construct()
	{
		$this->db = new Database;
		$this->table = "users";
	}

	public function agregarRegistro($data)
	{
		if ($this->db->insert("users", $data)) {
			$last_id = $this->db->lastInsertId();
			$this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id;");
			$this->db->bind(':id', $last_id, PDO::PARAM_INT);
			$result = $this->db->single();
			return $result;
		} else {
			return 0;
		}
	}

	public function usuarios()
	{
		$this->db->query("SELECT * FROM " . $this->table . ";");
		$resultados = $this->db->resultset();
		return $resultados;
	}

	public function actualizarRegistro($data, $id)
	{
		if ($this->db->updates($this->table, $data, $id)) {
			return true;
		} else {
			return false;
		}
	}

	public function eliminarRegistro($id)
	{
		if ($this->db->deletes($this->table, $id)) {
			return true;
		} else {
			return false;
		}
	}
}
```

### Acceder a los datos

Para acceder a los datos, se creará un objeto de la clase y se accederán a sus métodos:
Crear un archivo index.php y probar lo siguiente.

```
<?php
require('Init.php');

// se crea el objeto a partir de la clases.
$user = new User;

// dentro de la clase User se crearán los métodos necesarios
//Insertando registro
$newUser = [
	'name' => 'Jorge',
	'last_name' => 'Udemy',
	'email' => 'andrew@gmail.com',
	'gender' => 'Male',
	'hobbie' => 'Coding',
	'country' => 'Usa',
];

$usuarioNuew = $user->agregarRegistro($newUser);
echo $usuarioNuew->name . '<br>';
echo $usuarioNuew->last_name . '<br>';
echo $usuarioNuew->email . '<br>';


//Listando registros
echo '<table>
<thead>
  <tr>
	<th>#</th>
	<th>Nambe</th>
	<th>Last</th>
	<th>Email</th>
  </tr>
</thead>
<tbody>';

$i = 1;
foreach ($user->usuarios() as $u) {
	echo '<tr>
			<th scope="row">' . $i . '</th>
			<td>' . $u->name . '</td>
			<td>' . $u->last_name . '</td>
			<td>' . $u->email . '</td>
		</tr>';
	$i++;
}
echo '</tbody>
</table>';

//actualizar un registro
$datosUp['name'] = 'Jorge';
$datosUp['last_name'] = 'Nitales Axules';
$user->actualizarRegistro($datosUp, ['id' => 23]);

//eliminar un registro
$user->eliminarRegistro(4);
```

## License

License: MIT

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)