<?php
date_default_timezone_set('America/Mexico_City');

define("DB_HOST",         "localhost");
define("DB_USER",         "root");
define("DB_PASS",         "");
define("DB_NAME",         "database_test");
define("DB_PORT",         "330");
define("DB_CHARSET",     "latin1");
